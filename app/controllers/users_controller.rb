class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index,:edit,:update,:destroy]
  before_action :correct_user, only: [:edit,:update]
  before_action :admin_user, only: [:destroy]
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)#params[:user]
    if @user.save
      flash[:success] = "Create account"
      log_in(@user)
      redirect_to user_path(@user)#@userで＠user.idを渡していることになる。引数にuser objでデフォでidを渡す
    else
      render "new"
    end
  end
  
  def edit
  end
  
  def update
    if @user.update_attributes(user_params)
      flash[:success] = "success update your profile"
      redirect_to @user
    else
      render "edit"
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "success destroy"
    redirect_to users_path
  end
  
    private
    
    def user_params
      params.require(:user).permit(:name,:email,:password,:password_confirmation)
    end
    
    def logged_in_user
      unless logged_in?
       flash[:danger] = "please log in"
       store_location
       redirect_to login_path
      end
    end  
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to root_path unless current_user?(@user)
    end
    
    def admin_user
      redirect_to root_path unless current_user.admin?
    end
    
end
