class SessionsController < ApplicationController
  
  
  def new
    
  end
  
  #findだとidの検索になる為find_byを使用する
  #validationが使えない為flashを使用する
  def create
   @user = User.find_by(email: params[:session][:email].downcase)
   if @user && @user.authenticate(params[:session][:password])
     log_in(@user) #session[:user_id] = user.id
     params[:session][:remember_me] == "1"? remember(@user) : forget(@user) #remember_tokenをdigestに格納、cookiesにuser.id,remember_tokenを焼き付ける
     flash.now[:success] = "login success"
     redirect_back_or user_path(@user)
   else
     flash.now[:danger] ="login failure"
     render "new"
   end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_path
    flash[:success] = "Log out success"
  end
end