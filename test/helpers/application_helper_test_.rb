require "test_helper"

  test "full_title helper" do
    assert_equal full_title, "Ruby on Rails Tutorial Sample App"
    assert_equal full_title("Contact"), "Contact | Ruby on Rails Tutorial Sample App" 
  end