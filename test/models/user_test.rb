require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "example user",email: "user@example.com",
                     password: "foobar",password_confirmation: "foobar")
  end
  
   test "should be valid" do
    assert @user.valid?
  end

  
  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end
  
  test "should be presence email" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "name  should not be 50" do
    @user.name = "a"*51
    assert_not @user.valid?
  end
  
  test "email  should not be 255" do
    @user.email = "a"*244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "should be valid email addresses" do
    addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn]
    addresses.each do |address|
      @user.email = address
      assert @user.valid?,"#{address.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid email address" do
    addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    addresses.each do |address|
      @user.email = address
      assert_not @user.valid?,"#{address.inspect} should be invalid"
    end
  end
  
  test "email address should be unique" do
    dup_user = @user.dup
    dup_user.email = @user.email.upcase
    @user.save
    assert_not dup_user.valid?
  end
  
  test "email should be saved as downcase" do
    upcase_email = "UPCASE@EXAMPLE.COM"
    @user.email = upcase_email
    @user.save
    assert_equal upcase_email.downcase, @user.reload.email
    end
    
  test "password should be present" do
    @user.password = @user.password_confirmation = ""*6 
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a"*5
    assert_not @user.valid?
  end
  
  test "authenticate? should false with nil digest" do
    assert_not @user.authenticated?("")
  end
  
end
