require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
 
  def setup
    @user = users(:michael)
    @other= users(:archer)
  end
  
  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "should redirect edit when not logged in"do
    get edit_user_path(@user)
    assert_redirected_to login_path
    assert_not flash.empty?
  end
    
  test "should redirect update when not logged in " do
    patch user_path(@user),params: {user: {name: "foobar", email: "foobar@email.com",password: "",password_confirmation: ""}}
    assert_redirected_to login_path
    assert_not flash.empty?
  end

  test "should redirect edit when other user" do
    log_in_as(@other)
    get edit_user_path(@user)
    assert_redirected_to root_path
  end
  
  test "should redirect update when othre user"do
    log_in_as(@other)
    patch user_path(@user),params: {user: {name: "foobar",email: "foobar@email.com"}}
    assert_redirected_to root_path
  end

  test "should redirect index whem not logged in"do
    get users_path
    assert_redirected_to login_path
  end
  
  test "should not allow edit admin via web"do
    log_in_as(@other)
    assert_not @other.admin?
    patch user_path(@other),params: {user: {name: @other.name,email: @other.email,password: "password",
                                    password_confirmation: "password",admin: true}}
    assert_not @other.reload.admin?#reloadがないと変更が反映されない
  end

  test "should redirect destroy when not logged in"do 
    assert_no_difference "User.count"do
      delete user_path(@other)
    end
    assert_redirected_to login_path
  end

  test "should redirect destroy when not admin"do
    log_in_as(@other)
    assert_no_difference "User.count"do
      delete user_path(@user)
    end
    assert_redirected_to root_path
  end

end
