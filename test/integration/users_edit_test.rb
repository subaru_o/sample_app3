require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "invalid update" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template "users/edit"
    patch user_path(@user),params: {user: {name: "",email: "",password: "",password_confirmation: ""}}
    assert_template "users/edit"
  end
  
  test "valid update" do
    get edit_user_path(@user)
    assert_equal edit_user_url(@user),session[:forwarding_url]
    log_in_as(@user)
    assert_nil session[:forwarding_url]
    assert_redirected_to edit_user_url(@user)
    name = "foobarr"
    email = "foobar@email.com"
    patch user_path(@user),params: {user: {name: name,email: email,password: "",password_confirmation: ""}}
    assert_redirected_to user_path(@user)
    follow_redirect!
    @user.reload
    assert_not flash.empty?
    
    assert_equal name, @user.name
    assert_equal email, @user.email
    
  end
  
  
    
  
end
