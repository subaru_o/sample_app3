require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @other= users(:archer)
  end

  test "should exist index" do
    log_in_as(@user)
    get users_path
    assert_template "users/index"
    assert_select "div.pagination"
    User.paginate(page: 1).each do |user|
      assert_select "a[href=?]",user_path(user),text: user.name
      unless user.admin?
        assert_select "a[href=?]",user_path(user),method: :delete,text: "delete"
      end
    end
    assert_difference "User.count",-1 do
      delete user_path(@other)
    end
    assert_redirected_to users_path
    assert_not flash.empty?
  end
  
  test "index non admin" do
    log_in_as(@other)
    get users_path
    assert_select "a",text: "delete",count: 0
  end
  
end
