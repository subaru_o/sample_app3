require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  test "invalid create users" do
    get signup_path
    assert_select "form[action=?]","/signup"
    assert_no_difference "User.count"do
      post signup_path,params: {user: {name: "hoge",email: "hoge",
                              password: "foo",password_confirmation: "baz"}}
    end
    assert_template "users/new"
    assert_select "div.field_with_errors"
    assert_select "div#error_explanation"
  end
  
  test "valid create users" do
    get signup_path
    assert_difference "User.count",1 do
      post users_path,params: {user: {name: "foobar",email: "foobar@email.com",
                              password: "foobar",password_confirmation: "foobar"}}
    end
    assert is_logged_in?
    follow_redirect!
    assert_template "users/show"
    assert_not flash.empty?
  end
  
end
